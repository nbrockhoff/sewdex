<?php

session_start();
require_once('inc/fabric.class.php');


$searchValue = null;
if (isset($_GET['search']))
{
    $searchValue = filter_var($_GET['search'], FILTER_SANITIZE_STRING);
}   

$fabrics = new fabric();

$user_stash_list = $fabrics->getList($searchValue);

$columnsToDisplay = 
array(
    "Fabric" => "fabric_title",
    /* "Print" => "fabric_print_name",
    "Colorway" => "fabric_color",
    "Collection" => "fabric_collection",
    "Brand" => "fabric_brand",
    "Designer" => "fabric_designer", */
    "Type" => "fabric_substrate",
    "WOF" => "fabric_width",
    "Available" => "fabric_qty_available"
);

require_once('tpl/view_all_fabric.tpl.php');
?>