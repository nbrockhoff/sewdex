<?php
require_once('../inc/fabric.class.php');


$fabric = new fabric();

if (isset($_GET['fabric_id']) && $_GET['fabric_id'] >0)
{
    $fabric->load($_GET['fabric_id']);
} 
elseif (isset($_POST['fabric_id']) && $_POST['fabric_id'] >0) 
{
    $fabric->load($_POST['fabric_id']);
}

$dataValues = $fabric->data;


if (isset($_POST['btnUpdate']))
{
    unset($_POST['btnUpdate']);
    $fabric->set($_POST);
    $validFabric;
    
    if ($fabric->checkFabric()) 
    {
        if ($fabric->save()) 
        {
            header("location:../view_single_fabric.php?fabric_id=".$_POST['fabric_id']);
            exit;
        }
    }
}

        
        require_once('../tpl/edit_single_fabric.tpl.php'); ?>
