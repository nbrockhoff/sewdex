<?php
require_once('../inc/fabric.class.php');

echo ($_SESSION['sessionMessage']);

/* REFERENCE:
    if (isset($_POST['btnLogin']))
        { 
            if ($user->checkLogin($_POST['user_username'], $_POST['user_password']))
            {
                $_SESSION['sessionMessage'] = "<h4>Login Successful!</h4><h2>Welcome Back, ".$_SESSION['user_username']."!</h2>"; 
                
                header('location: view_all_fabric.php');
                exit;
                
            } 
            
        } */

if (isset($_POST['btnAdd']))
    {  unset($_POST['btnAdd']) ;
        
        $fabric = new fabric();   
        $fabric->set($_POST);
        $validFabric;
        $dataValues = $fabric->data;
   
        if($fabric->checkFabric()){
           
                if ($fabric->save()) 
                { echo "save success";
                    $_SESSION['sessionMessage'] = "<h4>Fabric added successfully!</h4>"; 
                    
                    header('location: ../view_all_fabric.php'); /* needs to go to single view */
                    exit;
                } else {
                    echo "save failure";
                     $_SESSION['sessionMessage'] = "<h4>Fabric was unable to be added.</h4>";
                }
                
            } else {
                $_SESSION['sessionMessage'] = "<h4>Fabric was unable to be added.<br><small>Please address the following errors before trying again.</small></h4>";
            }
        
}

        require_once('../tpl/add_new_fabric.tpl.php'); ?>
