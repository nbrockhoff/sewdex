<?php
require_once('../inc/fabric.class.php');


$deleteFabric = new fabric();

if (isset($_GET['fabric_id']) && $_GET['fabric_id'] >0)
{
    $deleteFabric->load($_GET['fabric_id']);
} 
elseif (isset($_POST['fabric_id']) && $_POST['fabric_id'] >0) 
{
    $deleteFabric->load($_POST['fabric_id']);
}

$dataValues = $deleteFabric->data;

if (isset($_POST['btnDelete']))
{
    unset($_POST['btnDelete']);
    $deleteFabric->set($_GET);
    echo 'delete fabric set';
    if(!$deleteFabric->fabricDelete()){
        echo "Fabric removed from stash";
    }
    
}

require_once('../tpl/delete_single_fabric.tpl.php'); ?>
