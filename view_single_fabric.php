
    <?php 
session_start();

/*
    <h1>Title of item</h1>
    <!-- able to edit Title data -->

    <!-- FUTURE: Display photo -->
    <h2>Fabric Print Name <small>color</small></h2>
    <h3>Collection, <em>WOF" Substrate</em></h3>
    <h4>Fabric Designer for Brand</h4>

    <h5>Available: available-yardage</h5>
    <h6>Used: used-yardage</h6>

    <form id="fabric-edit" name="resource-edit" method="get" action="">
        <?php
                    //Fields autopopulated using the fields from the linked public table,
                    //Fields unhidden if empty
                    ?>
            <p>
                <label for="fabric-name">Print name:</label>
                <input type="text" name="fabric-print" id="fabric-print"> </p>
            <p>
                <label for="fabric-name">Color:</label>
                <input type="text" name="fabric-color" id="fabric-color">
            </p>
            <p>
                <label for="fabric-name">Brand:</label>
                <input type="text" name="fabric-brand" id="fabric-brand">
            </p>
            <p>
                <label for="fabric-name">Fabric Designer:</label>
                <input type="text" name="fabric-designer" id="fabric-name">
            </p>
            <p>
                <label for="fabric-name">Fabric Collection:</label>
                <input type="text" name="fabric-collection" id="fabric-name"> </p>
            <p>
                <label for="fabric-title">Width of Fabric</label>
                <input type="text" name="fabric-width" id="fabric-width">
            </p>
            <!-- FUTURE: Additional Properties

                    <p><label for="fabric-title">Fabric Substrate</label>
                        <input type="text" name="fabric-substrate" id="fabric-substrate"></p>

                        -->
            <p>
                <input type="submit" name="update-resource-properties" id="update-resource-properties" value="Update Resource">
            </p>
            <?php
                    //edit resource uses the prepopulated fields from the linked public table row to create a new similar row in the private table.

                    //AJAX?
                ?>
                <hr>


                <form id="update-resource" name="update-resource" method="post" action="">

                    <p>
                        <label for="fabric-amount">Current Fabric Amount</label>
                        <input type="text" name="fabric-amount" id="fabric-amount">
                    </p>
                    <p>
                        <input type="submit" name="update-resource-amount" id="update-resource-amount" value="Update Amount">
                    </p>
                    <!-- FUTURE: Advanced Amount options

                    <p><label for="fabric-title">Fabric - Original Amount</label>
                        <textbox name="fabric-used-amount" id="fabric-original-amount">quilting cotton</p>

                    <p><label for="fabric-title">Fabric - Used Amount</label>
                        <textbox name="fabric-used-amount" id="fabric-used-amount">quilting cotton</p>
                -->
                    <button type="submit" name="fabric-update" id="fabric-update">Update Your Stash</button>
                </form>
                */ ?>
                
                <?php require_once('inc/fabric.class.php');

$user_stash_fabric = new fabric();

if (isset($_GET['fabric_id']) && $_GET['fabric_id'] >0)
{
    $user_stash_fabric->load($_GET['fabric_id']);
} 

$dataValues = $user_stash_fabric->data; ?>
                
                <?php require_once('tpl/view_single_fabric.tpl.php'); ?>

</body>

</html>
