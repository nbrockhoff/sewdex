<?php
require_once('base.class.php');

class fabric extends base 
{
    var $tableName = 'fabric';
    var $keyField = 'fabric_id';
    var $searchableFields = array(  
                                    'fabric_title',
                                    'fabric_print_name', 
                                    'fabric_color', 
                                    'fabric_collection', 
                                    'fabric_brand', 
                                    'fabric_designer', 
                                    'fabric_substrate', 
                                    'fabric_width', 
                                    'fabric_qty_available'
                                    );
                                    
    var $requiredField = array(
                                'fabric_title',
                                'fabric_width',
                                'fabric_qty_available'
                                );
    
    
    function sanitize($sanitizeData) 
    {
        return $sanitizeData;
    }
    
    function checkFabric()
    {
    //variables
        $fabricTitle=($_POST['fabric_title']);
        $fabricTitleErrorMsg;
        $fabricWidth=($_POST['fabric_width']);
        $fabricWidthErrorMsg;
        $fabricQtyAvailable=($_POST['fabric_qty_available']);
        $fabricQtyAvailableErrorMsg;
        $formDisplay;
        $validFabric;
        if (empty(trim($fabricTitle))==true){

          $validFabric = false;
          $fabricTitleErrorMsg="Please enter a title for your fabric.";
          //echo "Failed fabricTitle";
        } elseif ($fabricTitle != strip_tags($fabricTitle)){
          //echo "Failed fabricTitle";
          $validFabric=false;
          $fabricTitleErrorMsg="Please use alphanumeric characters only to enter your fabric title.";
          } else {
          $validFabric=true;
          //echo "Passed fabricTitle";
        };
        
        if($validFabric==true){
  
        if (empty(trim($fabricWidth))){
          $validFabric=false;
          $fabricWidthErrorMsg ="Please enter a width for your fabric.";
        
          //echo "Failed fabricWidth";
        } else {
          $validFabric=true;
          //echo "passed fabricWidth";
        };
      };
        
      if($validFabric==true){
          if (empty(trim($fabricQtyAvailable))){
            //echo "Failed fabricQty";
            $validFabric = false;
            $fabricQtyAvailableErrorMsg="Please enter an amount available for your fabric.";
          } elseif (!preg_match("/([0-9])+((\.)+([0-9]3)?)?/",$fabricQtyAvailable)){
            $validFabric = false;
            $fabricQtyAvailableErrorMsg="Please enter a valid decimal number.";
      //echo "Failed fabricQty";
          } else {
            $validFabric=true;
            //echo "passed fabricQty";
        };
      };
      
      
      
      return $validFabric;
    }
  
}
?>