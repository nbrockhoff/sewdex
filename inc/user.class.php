<?php
require_once('base.class.php');
require_once('validation.php');

class user extends base 
{
    var $tableName = 'users';
    var $keyField = 'user_id';
    var $searchableFields = array('user_id', 'user_username', 'user_level');
    
    function sanitize($sanitizeData) 
    {
        return $sanitizeData;
    }
    
    

    function checkRights($user_id, $user_level) 
    {
        $hasRights = false;

        $user = new users();
        
        if ($user->load($user_id)) 
        {
            $hasRights = ($user->data['user_level'] == $user_level);
        }

        return $hasRights;
    }
    
    function checkLogin($user_username, $user_password)
    {   
        $validUsername=false;
        $validPassword=false;
        $userMatch=false;
        
        if(empty($user_username)){
            $_SESSION['sessionMessage']="Please enter your username";
        } else {
            $validUsername=true;
        }
        
        if($validUsername==true){
            if(empty($user_password)){
                $_SESSION['sessionMessage']="Please enter your password";
            } else {
                $validPassword=true;
                
            }
        }
        
        if($validPassword==true){
        
            $stmt = $this->db->prepare("SELECT user_username FROM " . 
                        $this->tableName . " WHERE user_username = ?");
                $stmt->execute(array($user_username));
                if ($stmt->rowCount() == 1) 
                    {
                            $userMatch = true;
                    } else {
                        $_SESSION["sessionMessage"] = "Incorrect Username";
                    }
        }
        
        if ($userMatch==true){
            
            
                $success = false;
                $stmt = $this->db->prepare("SELECT user_id, user_level, user_username FROM " . 
                        $this->tableName . " WHERE user_username = ? AND user_password = ?");
                $stmt->execute(array($user_username, $user_password));
                if ($stmt->rowCount() == 1) 
                {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
                    if ($row['user_id'] > 0) 
                    
                    {
                        $success = true;
                        $_SESSION['user_id'] = $row['user_id'];
                        $_SESSION['user_username'] = $row['user_username'];
                        $_SESSION['user_level'] = $row['user_level'];
                        $_SESSION['is_valid_user'] = true;
                    } 
                } else {
                $_SESSION["sessionMessage"] = "Incorrect Password";
            }
                
                return $success;
            
        }
        }
    
    }


?>