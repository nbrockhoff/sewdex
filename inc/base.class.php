<?php
class base 
{
    var $data = array();
    var $db = null;
    var $errors = array();
    var $tableName = null;
    var $keyField = null;
    var $searchableFields = array();
        
    function __construct() 
    {
        $this->connectToDB();
    }
    
    function connectToDB() 
    {
        $success = true;
        
        $this->db = new PDO("mysql:host=localhost:3306;dbname=c9;charset=utf8", "wdv2016", "", array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
        
        if (!$this->db)
        {
            $errors[] = 'Unable to connect to database';
            $success = false;
        }
        
        
        return $success;
    }
        
    function set($setData) 
    {
        $this->data = $this->sanitize($setData);
    }
    
    function sanitize($sanitizeData) 
    {        
        return $sanitizeData;
    }
    
    function validate()
    {
        // validate $this->data
        
        return false;
    }
    
    function load($loadId)
    {
        $success = false;
        
        $stmt = $this->db->prepare("SELECT * FROM " . $this->tableName . " WHERE " . $this->keyField . " = ?");
        $stmt->execute(array($loadId));

        if ($stmt->rowCount() == 1) {
            $success = true;
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);        
            $this->data = $rows[0];
    
        }
        
        return $success;
    }
    
    function save()
    {
        $success = true;
        //var_dump($this->data);
        if (isset($this->data[$this->keyField]) && !empty($this->data[$this->keyField])) 
        {
            // update
            $updateSQL = "UPDATE " . $this->tableName . " SET ";
            
            $count = 0;
            foreach ($this->data as $columnName => $columnValue) 
            {
                $updateSQL .= ($count++ > 0 ? "," : "") . $columnName . " = ?";                
            }
            
            $updateSQL .= " WHERE " . $this->keyField . " = ?";
            
            $updateValues = array_values($this->data);
            $updateValues[] = $this->data[$this->keyField];
            
            //var_dump($updateSQL, $updateValues);
            
            $stmt = $this->db->prepare($updateSQL);
            
            //var_dump($updateSQL, $updateValues, $stmt);
            
            //var_dump($stmt->execute($updateValues));
            
            if (!$stmt->execute($updateValues))
            
            {
                $success = false;
                $this->errors[] = $stmt->errorInfo();
            }
        } 
        elseif (isset($this->data[$this->keyField]) && empty($this->data[$this->keyField]))
        {
            // insert
            $updateSQL = "INSERT INTO " . $this->tableName . " SET ";
            
            $count = 0;
            foreach ($this->data as $columnName => $columnValue) 
            {
                $updateSQL .= ($count++ > 0 ? "," : "") . $columnName . " = ?";                
            }
                        
            $updateValues = array_values($this->data);
            
            //var_dump($updateSQL, $updateValues);
            
            $stmt = $this->db->prepare($updateSQL);
            $stmt->execute($updateValues);
            
                $this->data[$this->keyField] = $this->db->lastInsertId();
                $newFabricID =  $this->data[$this->keyField];
                
            
        }
                
        return $success;
    }
    
    function fabricDelete(){
        
        $success = true;
        
        //var_dump($this->data);
        if (isset($this->data[$this->keyField]) && !empty($this->data[$this->keyField])) 
        {
            echo "Made it past first if";
            // update
            $deleteSQL = "DELETE FROM " . $this->tableName;
            
            /* $count = 0;
            foreach ($this->data as $columnName => $columnValue) 
            {
                $deleteSQL .= ($count++ > 0 ? "," : "") . $columnName . " = ?";                
            }
            */
            
            $deleteSQL .= " WHERE " . $this->keyField . " = ?";
            
            $deleteValues = array_values($this->data);
            
            var_dump($deleteSQL, $deleteValues);
            
            $stmt = $this->db->prepare($deleteSQL);
            
            var_dump($deleteSQL, $deleteValues, $stmt);
            
            var_dump($stmt->execute($deleteValues));
        }
            if (!$stmt->execute($deleteValues))
            
            {
                $success = false;
                $this->errors[] = $stmt->errorInfo();
            } else {
                header("location:../view_all_fabric.php");
            } 
        }
    
    function getList($searchValue = null) 
    {
        $listSQL = "SELECT * FROM " . $this->tableName;
        $parameterList = array();
        
         if (!is_null($searchValue) && !empty($searchValue))
        {
            $listSQL .= " WHERE ";
            
            $fieldCnt = 0;
            
            foreach ($this->searchableFields as $searchableField) 
            {
                $listSQL .= ($fieldCnt++ > 0 ? " OR " : "") .$searchableField . " LIKE ?";
                
                $parameterList[] = '%' . $searchValue . '%';
            }
        }
        
        $stmt = $this->db->prepare($listSQL);
        $stmt->execute($parameterList);
        
        return $stmt;
    }

    function getListArray() 
    {
        $stmt = $this->db->prepare("SELECT * FROM " . $this->tableName);
        $stmt->execute();
        
        return $stmt->fetchAll(PDO::FETCH_ASSOC);        
    }

   
    



}
?>
