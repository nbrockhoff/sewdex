<html>   
    <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>

#custom-search-input {
        margin:0;
        margin-top: 10px;
        padding: 0;
    }
 
    #custom-search-input .search-query {
        padding-right: 3px;
        padding-right: 4px \9;
        padding-left: 3px;
        padding-left: 4px \9;
        /* IE7-8 doesn't have border-radius, so don't indent the padding */
 
        margin-bottom: 0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }
 
    #custom-search-input button {
        border: 0;
        background: none;
        /** belows styles are working good */
        padding: 2px 5px;
        margin-top: 2px;
        position: relative;
        left: -28px;
        /* IE7-8 doesn't have border-radius, so don't indent the padding */
        margin-bottom: 0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        color:#D9230F;
    }
 
    .search-query:focus + button {
        z-index: 3;   
    }
</style>
</head>

    <body>
        
        <div class="session-message">
			     <h3><?php 
			             echo ($_SESSION['sessionMessage']);
			             $_SESSION['sessionMessage']=""
			             ?></h3>
			     </div>
            <?php if($_SESSION['is_valid_user']==false){ ?>
        <a href="../user_login.php"><h4>Log In to Manage Your Fabric</h4></a><br>
                   <?php }; ?>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2>Available Fabric Stash</h2>
                </div>
            <form action="view_all_fabric.php" method="get">
                <div id="custom-search-input">
                    <div class="input-group col-md-4">
                        <input type="text" name="search" class="search-query form-control" placeholder="Search" value="<?php echo $searchValue; ?>" />
                                <span class="input-group-btn">
                                    <button class="btn" type="button">
                                        <span class=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                    </div>
                </div>
            </form>
            <h4><a href="users/add_new_fabric.php">Add A New Fabric</a></h4>
        <table class="table table-hover">
            <thead>
                <tr>
                    <?php foreach ($columnsToDisplay as $description => $column)
                    { ?>
                        <th><?php echo $description; ?></th>
                    <?php } ?>
                        <th colspan="2"></th>
                            
                </tr>
            </thead>
            <?php while($row = $user_stash_list->fetch(PDO::FETCH_ASSOC)) 
            { ?>
                 <tbody>   <tr>
                        <?php foreach ($columnsToDisplay as $description => $column)
                        {?>
                            <td><?php echo $row[$column]; ?></td>                    
                        <?php } ?>
                       <?php if($_SESSION['is_valid_user']==true)
                            { ?>  
                        <td>
                            <a href="view_single_fabric.php?fabric_id=<?php echo $row['fabric_id']; ?>">View Details</a>
                        </td>
                        <td>
                            <a href="users/edit_single_fabric.php?fabric_id=<?php echo $row['fabric_id']; ?>">Edit Fabric</a>
                        </td>
                        <td>
                            <a href="users/delete_single_fabric.php?fabric_id=<?php echo $row['fabric_id']; ?>">Delete Fabric</a>
                        </td>
                        <?php } ?>
                    </tr>
                    </tbody>
            <?php } ?>
            
        </table>
        </div>
        </div>
        
        
        <h4><a href="users/logout.php">Log Out</a></h4>
    </body>        
</html>