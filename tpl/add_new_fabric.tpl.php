<?php
      $valid;
      $formDisplay;
?>
<html>
    <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        .edit-fabric,
        .delete-fabric {
            margin-bottom: 15px;
        }
    </style>
</head>
<body>
    <?php if (isset($fabric)){ ?>
    <div class="session-message">
        <h3><?php 
			             echo ($_SESSION['sessionMessage']);
			             $_SESSION['sessionMessage']="";
			             ?></h3>
    </div>
    <?php } ?>
    <div id="content" class="container">
        <div class="row">
            <div class="col-xs-12">
                <!-- <div class="search">
                <div>
                    <form id="search-box" name="search-box" method="get" action="stash_search.php">
                        <input type="text" name="search-box" id="search-box">
                        <button type="submit" name="fabric-search" id="fabric-search">Search for Fabric</button>
                    </form>
                </div>
            </div>-->

                <div class="add-new-fabric-details">
                    <h1>Add New Fabric</h1>
                    <hr>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <form class="form-horizontal" action="add_new_fabric.php" method="POST">
                    <input type="hidden" name="fabric_id" value="<?php (isset($dataValues['fabric_id']) ? $dataValues['fabric_id'] : ""); ?>"/>
                    <br>
                    <div class="form-group"> 

                        <label for="fabric-title" class="col-sm-2 control-label">Fabric Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-title" name="fabric_title" value="<?php echo (isset($dataValues['fabric_title']) ? $dataValues['fabric_title'] : ""); ?>">
                        
                        </div>
                    </div>
                    <br><?php echo $fabricTitleErrorMsg; ?>
                    <div class="form-group">
                        <label for="fabric-print-name" class="col-sm-2 control-label">Fabric Print Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-print-name" name="fabric_print_name" value="<?php echo (isset($dataValues['fabric_print_name']) ? $dataValues['fabric_print_name'] : ""); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fabric-color" class="col-sm-2 control-label">Fabric Colorway</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-color" name="fabric_color" value="<?php echo (isset($dataValues['fabric_color']) ? $dataValues['fabric_color'] : ""); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fabric-collection" class="col-sm-2 control-label">Fabric Collection</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-collection" name="fabric_collection" value="<?php echo (isset($dataValues['fabric_collection']) ? $dataValues['fabric_collection'] : ""); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fabric-width" class="col-sm-2 control-label">Fabric Width</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-width" name="fabric_width" value="<?php echo (isset($dataValues['fabric_width']) ? $dataValues['fabric_width'] : ""); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fabric-width" class="col-sm-2 control-label">Fabric Substrate</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-substrate" name="fabric_substrate" value="<?php echo (isset($dataValues['fabric_substrate']) ? $dataValues['fabric_substrate'] : ""); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fabric-designer" class="col-sm-2 control-label">Fabric Designer</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-designer" name="fabric_designer" value="<?php echo (isset($dataValues['fabric_designer']) ? $dataValues['fabric_designer'] : ""); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fabric-brand" class="col-sm-2 control-label">Fabric Brand</label>
                        <div class="col-sm-10">
                            
                            <input type="text" class="form-control" id="fabric-brand" name="fabric_brand" value="<?php echo (isset($dataValues['fabric_brand']) ? $dataValues['fabric_brand'] : ""); ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="fabric-qty-available" class="col-sm-2 control-label">Available Yardage</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-qty-available" name="fabric_qty_available" value="<?php echo (isset($dataValues['fabric_qty_available']) ? $dataValues['fabric_qty_available'] : ""); ?>">
                        </div>
                        <?php echo $fabricQtyAvailableErrorMsg;?>
                    </div>
                    <div class="form-group">
                        <label for="fabric-qty-original" class="col-sm-2 control-label">Original Yardage</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-qty-original" name="fabric_qty_original" value="<?php echo (isset($dataValues['fabric_qty_original']) ? $dataValues['fabric_qty_original'] : ""); ?>">
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label for="fabric-qty-used" class="col-sm-2 control-label">Used Yardage</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fabric-qty-used" name="fabric_qty_used" value="<?php echo (isset($dataValues['fabric_qty_used']) ? $dataValues['fabric_qty_used'] : ""); ?>">
                        </div>
                    </div>
                    </div>

            <div class="col-xs-12 col-md-4">
                <div class="edit-fabric">
                    <input class="btn btn-primary" type="submit" value="Add New Fabric" name="btnAdd">
                </div>
            </div>
        </div>
    </div>

</body>

</html>