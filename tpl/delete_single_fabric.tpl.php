<html>
    <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        .btn{
            margin-bottom: 15px;
        }
    </style>
</head>
<body><div class="session-message">
			     <h3><?php 
			             echo ($_SESSION['sessionMessage']);
			             $_SESSION['sessionMessage']=""
			             ?></h3>
			     </div>
        <div id="content" class="container">
            <div class="row">
                <div class="col-xs-12">
            <!-- <div class="search">
                <div>
                    <form id="search-box" name="search-box" method="get" action="stash_search.php">
                        <input type="text" name="search-box" id="search-box">
                        <button type="submit" name="fabric-search" id="fabric-search">Search for Fabric</button>
                    </form>
                </div>
            </div>-->
                
                    <div class="view-fabric-details">
                        <!-- Title --><h1><?php echo (isset($dataValues['fabric_title']) ? $dataValues['fabric_title'] : ""); ?></h1>
                    <hr>
                    </div>
                </div>
            </div>
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <!-- Print --><h2><?php echo (isset($dataValues['fabric_print_name']) ? $dataValues['fabric_print_name'] : ""); ?> 
                        <!-- Color --><small>(<?php echo (isset($dataValues['fabric_color']) ? $dataValues['fabric_color'] : ""); ?>)</small></h2>
                        <!-- Collection --><h3><?php echo (isset($dataValues['fabric_collection']) ? $dataValues['fabric_collection'] : ""); ?>, 
                        <!-- WOF --><em><?php echo (isset($dataValues['fabric_width']) ? $dataValues['fabric_width'] : ""); ?>"  
                        <!-- Substrate --><?php echo (isset($dataValues['fabric_substrate']) ? $dataValues['fabric_substrate'] : ""); ?></em></h3>
                        <!-- Designer --><h4><?php echo (isset($dataValues['fabric_designer']) ? $dataValues['fabric_designer'] : ""); ?> 
                        <!-- Brand -->for <?php echo (isset($dataValues['fabric_brand']) ? $dataValues['fabric_brand'] : ""); ?></h4>

                        <!-- Available yardage --><h5><?php echo (isset($dataValues['fabric_qty_available']) ? $dataValues['fabric_qty_available'] : ""); ?> yards available</h5>
                        <!-- Used yardage --><h6><?php echo (isset($dataValues['fabric_qty_used']) ? $dataValues['fabric_qty_used'] : ""); ?> yards used</h6>
                    </div>
                    
                    <div class="col-xs-12 col-md-4">
                        
                        <div class="delete-fabric">
                            <form action="delete_single_fabric.php?fabric_id=<?php echo $dataValues['fabric_id']; ?>" method="POST">
                            <h1 class="text-danger">Are you sure you want to remove this fabric from your stash?</h1>
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <input type="submit" class="btn btn-danger" name="btnDelete" value="Delete Fabric">
                            </div>
                            <div class="col-xs-12 col-md-6">
                            <div class="cancel-edit">
                                <a class="btn btn-default" href="../view_single_fabric.php?fabric_id=<?php echo $_GET['fabric_id']; ?>" role="button">Cancel</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                        </form>
                        </div>
               </div>
               </div>
               
           
                    
                    
                    
                    
            </div>
        </div>
        
        <h4><a href="view_all_fabric.php">Return to Full Stash</a></h4>
        <h4><a href="users/logout.php">Log Out</a></h4>
</body>

</html>
