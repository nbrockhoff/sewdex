<html>
    <head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
    <div class="row">
		<div class="col-md-6 end">
			<form class="form-horizontal" action='user_login.php' method="POST">
			  <fieldset>
			    <div id="legend">
			      <legend class="">Login</legend>
			    </div>
			    <div class="session-message">
			     <h3><?php echo ($_SESSION['sessionMessage']);?></h3>
			     </div>
			     <div class = "row">
			         <div class="col-md-8 col-md-offset-2">
    			    <div class="form-group">
    			      <!-- Username -->
    			      <label c for="username">Username</label>
    			      <div class="controls">
    			        <input type="text" id="user_username" name="user_username" placeholder="" class="form-control input-xlarge " value="<?php echo (isset($data['user_username']) ? $dataValues['user_username'] : ""); echo $_POST['user_username']?>">
    			        <div class="errors-username"> <?php echo $errors['user_username'] ?></div>
    			      </div>
    			    </div>
    			    <div class="form-group">
    			      <!-- Password-->
    			      <label for="password">Password</label>
    			      <div class="controls">
    			        <input type="password" id="user_password" name="user_password" placeholder="" class="form-control input-xlarge" value="<?php echo (isset($data['user_password']) ? $dataValues['user_password'] : ""); ?>">
    			      </div>
    			    </div>
    			    <div class="form-group">
    			      <!-- Button -->
    			      <div class="controls">
    			        <input type="submit" name="btnLogin" class="btn btn-success" value="Login" />
    			      </div>
			      </div>
			    </div>
			  </fieldset>
			</form>
		</div>
	</div>
</div>
       
           <!--
            username: <input type="text" name="user_username" value=""/>
                
            password: <input type="password" name="user_password" value=""/><br>
            <input type="submit" name="btnLogin" value="Login"/><input type="submit" name="btnCancel" value="Cancel"/>
        </form> -->
    </body>
</html>